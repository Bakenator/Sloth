defmodule Sloth.Repo.Migrations.CreateClicks do
  use Ecto.Migration

  def change do
    create table(:clicks) do
      add :post_id, :integer

      timestamps()
    end

  end
end
