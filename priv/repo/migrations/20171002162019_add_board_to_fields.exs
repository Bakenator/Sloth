defmodule Sloth.Repo.Migrations.AddBoardToFields do
  use Ecto.Migration

  def change do
    alter table(:messages) do
      add :board_id, :integer, default: -1
    end

    alter table(:notes) do
      add :board_id, :integer, default: -1
    end

    alter table(:sentries) do
      add :board_id, :integer, default: -1
    end

    alter table(:sections) do
      add :board_id, :integer, default: -1
    end
  end
end
