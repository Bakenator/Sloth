export const initBoards = board_list => {
  return {
    type: 'INIT_BOARDS',
    board_list: board_list
  }
}

export const initMentions = mention_list => {
  return {
    type: 'INIT_MENTIONS',
    mention_list: mention_list
  }
}

export const addMentions = new_mention_list => {
  return {
    type: 'ADD_MENTIONS',
    new_mention_list: new_mention_list
  }
}