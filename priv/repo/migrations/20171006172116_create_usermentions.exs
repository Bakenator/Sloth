defmodule Sloth.Repo.Migrations.CreateUsermentions do
  use Ecto.Migration

  def change do
    create table(:usermentions) do
      add :user_id, :integer
      add :message_id, :integer

      timestamps()
    end

  end
end
