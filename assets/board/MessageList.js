import React from 'react';
import { connect } from 'react-redux'
import ChatMessage from './ChatMessage';
import ContentEditable from "react-contenteditable";
import ReactDOM from "react-dom"
import { updateChatText } from "./actions";
import { process_tags } from './chat_processing';

class MessageList extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      endOfLine: false
    };
  }


  chatEnter(e) {
    if (e.charCode === 13) {
      e.preventDefault();
      this.props.channel.push("new_msg", {body: this.props.chatText, board_id: this.props.board_id})
      this.checkForMessageActions(this.props.chatText, this.props.users);
      this.props.onChatUpdate("");
    }
  }

  checkForMessageActions(text, user_list) {
    var stripped_text = text;
    var assigned_user = -1;
    user_list.map((user) => 
                  { if (stripped_text.includes('@' + user.email)) { 
                      assigned_user = user.id; 
                      // stripped_text = stripped_text.replace('@' + user.email,'');
                    }
                  });
    if (text.includes('@addnote')) {
      stripped_text = stripped_text.replace('@addnote','');
      this.props.channel.push("note_create", {note_event: {"note_id": -1, "newX": 10, "newY": 10, "text": stripped_text, "assignee_id": assigned_user, "board_id": this.props.board_id}});
    } else if (text.includes('@addsection')) {
      stripped_text = stripped_text.replace('@addsection','');
      this.props.channel.push("section_create", {section: {title: stripped_text}, board_id: this.props.board_id});
    } else if (assigned_user != -1) {
      
    }
  }

  emitChange(e) {
    this.props.onChatUpdate(process_tags(e.target.value, this.props.users));
  }

  getElement(element) {
    return ReactDOM.findDOMNode(this)
  }


  placeCaretAtEnd(el) {
    el.focus();
    if (typeof window.getSelection != "undefined" && typeof document.createRange != "undefined") {
        var range = document.createRange();
        range.selectNodeContents(el);
        range.collapse(false);
        var sel = window.getSelection();
        sel.removeAllRanges();
        sel.addRange(range);
    } else if (typeof document.body.createTextRange != "undefined") {
        var textRange = document.body.createTextRange();
        textRange.moveToElementText(el);
        textRange.collapse(false);
        textRange.select();
        }
    }

  componentWillUpdate(nextProps, nextState) {
    let stater = this.state.endOfLine;
    if (!stater) {
      let chat = this.getElement(this.refs.chatInput).children[1].children[0];
      if (chat.innerHTML.length != nextProps.chatText.length) {
        this.setState({
          endOfLine: true,
        })
      }
    }
  }

  componentDidUpdate(prevProps, prevState) {
    if (this.state.endOfLine) {
      this.setState({
        endOfLine: false,
      });
      let chat = this.getElement(this.refs.chatInput).children[1].children[0];
      this.placeCaretAtEnd(chat);
    }

    if (prevProps.messages.length != this.props.messages.length) {
      let message_scroll = document.getElementsByClassName('chat_display')[0]
      message_scroll.scrollTop = message_scroll.scrollHeight;
    }
  }

  render() {
    const messagesList = this.props.messages.map((message) => {
                            return (<ChatMessage key={message.id} {...message} />)
                          })
    return ( 
      <div className="wholeChatArea"> 
        <div className="chat_display">
          <div>{messagesList}</div>
        </div>
        <div className="input_display">
          <ContentEditable
                      ref="chatInput"
                      onKeyPress={this.chatEnter.bind(this)}
                      className="message_input"
                      html={this.props.chatText} // innerHTML of the editable div
                      disabled={false}       // use true to disable edition
                      onChange={this.emitChange.bind(this)} // handle innerHTML change
                    />
         
        </div>
      </div>
    );
  }
}

const mapStateToProps = (state) => (state)

const mapDispatchToProps = dispatch => {
  return {
    onChatUpdate: text => {
      dispatch(updateChatText(text))
    }
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(MessageList)