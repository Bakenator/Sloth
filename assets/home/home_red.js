import { combineReducers } from 'redux'
import React from 'react';
// import messagers from './messagers'


const boards = (state = [], action) => {
  switch (action.type) {
    case 'INIT_BOARDS':
      return action.board_list
    default:
      return state
  }
}

const channel = (state = [], action) => {
  return state
}

const mentions = (state = [], action) => {
  switch (action.type) {
    case 'INIT_MENTIONS':
      return action.mention_list
    case 'ADD_MENTIONS':
      return state.concat(action.new_mention_list)
    default:
      return state
  }
}


const homeApp = combineReducers({
  // messagers
  boards,
  channel,
  mentions
})

export default homeApp