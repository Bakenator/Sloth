defmodule Sloth.Booms do
  @moduledoc """
  The Boards context.
  """

  import Ecto.Query, warn: false
  alias Sloth.Repo

  alias Sloth.Booms.Post
  alias Sloth.Booms.Click


  def create_post(attrs \\ %{}) do
    %Post{}
    |> Post.changeset(attrs)
    |> Repo.insert()
  end

  def create_click(attrs \\ %{}) do
    %Click{}
    |> Click.changeset(attrs)
    |> Repo.insert()
  end



end
