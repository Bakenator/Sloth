defmodule SlothWeb.Router do
  use SlothWeb, :router

  pipeline :browser do
    plug :accepts, ["html"]
    plug :fetch_session
    plug :fetch_flash
    plug :protect_from_forgery
    plug :put_secure_browser_headers

    #this assigns the current_user to conn
    plug Doorman.Login.Session
  end

  pipeline :api do
    plug :accepts, ["json"]
  end

  pipeline :restricted do
    plug SlothWeb.RequireLogin
  end

  scope "/", SlothWeb do
    pipe_through :browser # Use the default browser stack

    resources "/notes", NoteController
    resources "/messages", MessageController
    resources "/posts", PostController
    resources "/clicks", ClickController
    resources "/users", UserController
    resources "/sessions", SessionController
    resources "/boards", BoardController
    get "/login", SessionController, :new
    get "/sign_up", UserController, :new
  end

  scope "/", SlothWeb do
    pipe_through :browser # Use the default browser stack
    pipe_through :restricted

    get "/", PageController, :index
    get "/home", PageController, :home

    get "/reddit", BoomController, :home
  end

  # Other scopes may use custom stacks.
  # scope "/api", SlothWeb do
  #   pipe_through :api
  # end
end
