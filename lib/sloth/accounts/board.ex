defmodule Sloth.Accounts.Board do
  use Ecto.Schema
  import Ecto.Changeset
  alias Sloth.Accounts.Board

  import Ecto.Query
  alias Sloth.Repo
  alias Sloth.Boards.Message
  alias Sloth.Boards.Section
  import IEx



  schema "boards" do
    field :name, :string
    field :parent_board_id, :integer, default: nil
    field :closed, :boolean, default: false

    timestamps()
  end

  @doc false
  def changeset(%Board{} = board, attrs) do
    board
    |> cast(attrs, [:name, :parent_board_id, :closed])
    |> validate_required([:name])
    |> unique_constraint(:name)
  end

  def add_section_counts(board_map) do
    board_id = board_map.id
    sections = from(p in Section, where: p.board_id == ^board_id)
      |> Repo.all

    counts = sections
      |> Enum.map(fn(section) -> 
            section_length = length(Section.sentry_list(section, section.head_sentry_id))
            %{title: section.title, length: section_length}
          end)
    Map.put(board_map, :section_counts, counts)        
  end


  def notify_parent_board("close", parent_board_id, board_id, user) when parent_board_id != nil do
    changeset = Message.changeset(%Message{}, %{content: "#{user.email} has closed sub-board ##{board_id}", user_id: user.id, board_id: parent_board_id })
    {:ok, message} = Sloth.Repo.insert(changeset)
    
    msg = %{id: message.id, content: message.content, username: user.email}

    SlothWeb.Endpoint.broadcast("room:"<>Integer.to_string(parent_board_id), "new_msg", msg)
  end

  def notify_parent_board(_action_string, _parent_board_id, _board_id, user) do
  end

  def get_boards_for_home() do
    Sloth.Repo.all(Board)
      |> Enum.filter(fn(x) -> !x.closed end)
      |> Enum.map(fn(x) -> %{id: x.id, name: x.name, parent_board_id: x.parent_board_id} end)
      |> Enum.map(fn(x) -> Board.add_section_counts(x) end)
  end
end
