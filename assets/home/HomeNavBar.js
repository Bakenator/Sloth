import React from 'react';
import ReactDOM from "react-dom"
import ContentEditable from "react-contenteditable";

// https://stackoverflow.com/questions/20926551/recommended-way-of-making-react-component-div-draggable

class HomeNavBar extends React.Component {

  newBoardClick() {
    this.props.channel.push("new_board");
  }



  render() {
    return (
              <div className="home_nav_bar">
                <p className="home_nav_create_button" onClick={this.newBoardClick.bind(this)}>Create New Board</p>
              </div>
            ) 
  }
}

export default HomeNavBar