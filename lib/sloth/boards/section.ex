defmodule Sloth.Boards.Section do
  use Ecto.Schema
  import Ecto.Changeset
  alias Sloth.Boards.Section
  alias Sloth.Accounts.Sentry
  import Ecto.Query
  import IEx


  schema "sections" do
    field :position, :integer, default: 0
    field :title, :string
    field :head_sentry_id, :integer
    field :board_id, :integer

    timestamps()
  end

  @doc false
  def changeset(%Section{} = section, attrs) do
    section
    |> cast(attrs, [:board_id, :title, :position, :head_sentry_id])
    |> validate_required([:board_id, :title])
  end

  def sentry_list(%Section{} = section, head_sentry_id) when head_sentry_id == nil do 
    []
  end

  def raw_sentry_list(%Section{} = section, head_sentry_id) when head_sentry_id == nil do 
    []
  end

  def sentry_list(%Section{} = section, head_sentry_id) do
    sentry = Sloth.Repo.get(Sloth.Accounts.Sentry, head_sentry_id)
    stripped_sentry = Map.delete(Map.from_struct(sentry), :__meta__)  
    Sloth.Accounts.Sentry.get_sentry_list(sentry.next_sentry, [stripped_sentry])
  end

  def raw_sentry_list(%Section{} = section, head_sentry_id) do
    sentry = Sloth.Repo.get(Sloth.Accounts.Sentry, head_sentry_id)
    Sloth.Accounts.Sentry.get_raw_sentry_list(sentry.next_sentry, [sentry])
  end

  def add_note(section, curr_section_list, new_note_id) when curr_section_list == [] do
    {:ok, new_last_sentry} = Sentry.changeset(%Sentry{}, %{next_sentry: -1, note_client_id: new_note_id, section_id: section.id, board_id: section.board_id})
                            |> Sloth.Repo.insert                   


    # [section] = Enum.take((Sloth.Repo.all(Section)), 1)
    change = Section.changeset(section, %{head_sentry_id: new_last_sentry.id})
    {:ok, new_section} = Sloth.Repo.update(change)

    Section.sentry_list(new_section, new_section.head_sentry_id)
  end

  def add_note(section, curr_section_list, new_note_id) do
    # last_sentry = Sloth.Repo.one(from x in Sentry, where: x.next_sentry == -1, order_by: [desc: x.id], limit: 1)
    [last_sentry | _d] = Enum.take(curr_section_list, -1)

    {:ok, new_last_sentry} = Sentry.changeset(%Sentry{}, %{next_sentry: -1, note_client_id: new_note_id, section_id: section.id, board_id: section.board_id})
                            |> Sloth.Repo.insert                   

    change = Sentry.changeset(last_sentry, %{next_sentry: new_last_sentry.id})

    Sloth.Repo.update(change)
    Section.sentry_list(section, section.head_sentry_id)
  end

end
