// NOTE: The contents of this file will only be executed if
// you uncomment its entry in "assets/js/app.js".

// To use Phoenix channels, the first step is to import Socket
// and connect at the socket path in "lib/web/endpoint.ex":
import {Socket} from "phoenix"
import React from 'react';
import ReactDOM from "react-dom"
import UserDropDownList from './UserDropDownList';
import MessageList from './MessageList';
import NoteList from './NoteList';

let socket = new Socket("/socket", {params: {token: window.userToken}})

// When you connect, you'll often need to authenticate the client.
// For example, imagine you have an authentication plug, `MyAuth`,
// which authenticates the session and assigns a `:current_user`.
// If the current user exists you can assign the user's token in
// the connection for use in the layout.
//
// In your "lib/web/router.ex":
//
//     pipeline :browser do
//       ...
//       plug MyAuth
//       plug :put_user_token
//     end
//
//     defp put_user_token(conn, _) do
//       if current_user = conn.assigns[:current_user] do
//         token = Phoenix.Token.sign(conn, "user socket", current_user.id)
//         assign(conn, :user_token, token)
//       else
//         conn
//       end
//     end
//
// Now you need to pass this token to JavaScript. You can do so
// inside a script tag in "lib/web/templates/layout/app.html.eex":
//
//     <script>window.userToken = "<%= assigns[:user_token] %>";</script>
//
// You will need to verify the user token in the "connect/2" function
// in "lib/web/channels/user_socket.ex":
//
//     def connect(%{"token" => token}, socket) do
//       # max_age: 1209600 is equivalent to two weeks in seconds
//       case Phoenix.Token.verify(socket, "user socket", token, max_age: 1209600) do
//         {:ok, user_id} ->
//           {:ok, assign(socket, :user, user_id)}
//         {:error, reason} ->
//           :error
//       end
//     end
//
// Finally, pass the token on connect as below. Or remove it
// from connect if you don't care about authentication.

socket.connect()

// Now that you are connected, you can join channels with a topic:
let channel = socket.channel("room:" + board_id, {})
let chatInput         = document.querySelector("#chat-input")
let messagesContainer = document.querySelector("#messages")

// chatInput.addEventListener("keypress", event => {
//   if(event.keyCode === 13){
//     event.preventDefault();

//     channel.push("new_msg", {body: clearCommands(chatInput.innerHTML)})
//     checkAddNote(chatInput.innerHTML);

//     chatInput.innerHTML = ""
//   }
// })


channel.on("presence_state", payload => {
  console.log(payload);
})

channel.on("presence_diff", payload => {
  console.log(payload);
})

channel.on("new_msg", payload => {
  store.dispatch(addMessage(payload))
})

channel.on("note_move", payload => {
  console.log(payload);
  store.dispatch(moveNote(payload.note_event))
})

channel.on("note_assign", payload => {
  console.log(payload);
  store.dispatch(changeAssignee(payload.note_event.client_id, payload.note_event.assignee))
})

channel.on("note_create", payload => {
  console.log(payload);
  store.dispatch(createNote(payload.new_sentry_list, payload.note_event));
})

channel.on("section_create", payload => {
  console.log(payload);
  store.dispatch(createSection(payload.section_list));
})

channel.on("sentry_update", payload => {
  console.log(payload)
  store.dispatch(initSections(payload.new_sections));
})

channel.on("board_close", payload => {
  console.log(payload)
  console.log("this baord has been closed");
  window.location = '/home';
})


function checkAddNote(text) {
  if (text.includes('@addnote ')) {
    createNewNote(-1,20,20,clearCommands(text));
    return;
  }

  if (text.includes('@addnote')) {
    createNewNote(-1,20,20,clearCommands(text));
    return;
  }
}

function clearCommands(text) {
  if (text.includes('@addnote ')) {
    return text.replace("@addnote ", "");
  }
  if (text.includes('@addnote')) {
    return text.replace("@addnote", "");
  }
  return text;
}

function scrubHTML(text) {
  return text.replace(/<\/?[^>]+(>|$)/g, "");
}


import { Provider } from 'react-redux'
import { createStore } from 'redux'
import messageApp from './red_index';
import { initMessages } from './actions'
import { initSections } from './actions'
import { initNotes } from './actions'
import { initUsers } from './actions'
import { initBoardId } from './actions'
import { addMessage } from './actions'
import { moveNote } from './actions'
import { changeAssignee } from './actions'
import { createNote } from './actions'
import { createSection } from './actions'
import AppHolder from './AppHolder'

const initialState = { 
  messages: [],
  notes: [], 
  channel: channel, 
  chatText: "",
  selected_note: null
}

let store = createStore(messageApp, initialState);
ReactDOM.render(
  <Provider store={store}>
    <AppHolder />
  </Provider>,
  document.getElementById('messages')
);

channel.join()
  .receive("ok", resp => { 
    store.dispatch(initMessages(resp["room"]));
    store.dispatch(initNotes(resp["notes"]));
    store.dispatch(initUsers(resp["users"]));
    store.dispatch(initSections(resp["sections"]));
    store.dispatch(initBoardId(resp["board_id"]));


    console.log("Joined successfully", resp) })
  .receive("error", resp => { console.log("Unable to join", resp) })

export default socket
