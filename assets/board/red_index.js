import { combineReducers } from 'redux'
import React from 'react';
// import messagers from './messagers'


const messages = (state = [], action) => {
  switch (action.type) {
    case 'INIT_MESSAGES':
      return action.message_list
    case 'ADD_MESSAGE':
      return [...state, action.message]
    default:
      return state
  }
}

const selected_note = (state = [], action) => {
  switch (action.type) {
    case 'HOLDER_NOTE':
      return action.client_id
    case 'KILL_HOLDER_NOTE':
      return state
    case 'END_HOLDER_NOTE':
      return null
    default:
      return state
  }
}

const board_id = (state = [], action) => {
  switch (action.type) {
    case 'INIT_BOARD_ID':
      return action.board_id
    default:
      return state
  }
}

const notes = (state = [], action) => {
  switch (action.type) {
    case 'INIT_NOTES':
      return action.note_list.map((note) => {note.user_toggle = false; return note })
    case 'MOVE_NOTE':
      let new_state = state.map(function(note) {
                                if (note.client_id == action.note.client_id) {
                                  note.posX = action.note.posX;
                                  note.posY = action.note.posY;
                                  note.text = action.note.text;
                                  return note;
                                } else {
                                  return note;
                                }
                              }
                      )
      return new_state

    case 'CREATE_NOTE':
      let create_note = action.note;
      create_note.user_toggle = false;
      return [...state, create_note]

    case 'TOGGLE_USER_ASSIGN':
      let new_toggle_state = state.map(function(note) {
                                if (note.client_id == action.note_id) {
                                  let new_note = Object.assign({}, note)
                                  new_note.user_toggle = (!(new_note.user_toggle))
                                  return new_note;
                                } else {
                                  return note;
                                }
                              }
                      )
      return new_toggle_state
    case 'CHANGE_ASSIGNEE':
      let new_assignee_state = state.map(function(note) {
                                if (note.client_id == action.client_id) {
                                  let new_note = Object.assign({}, note)
                                  new_note.user_toggle = false;
                                  new_note.assignee = action.assignee
                                  new_note.assignee_id = action.assignee.id
                                  return new_note;
                                } else {
                                  return note;
                                }
                              }
                      )
      return new_assignee_state

    case 'HOLDER_NOTE':
      let new_holder_array = state.map(function(note) {
                                if (note.client_id == action.client_id) {
                                  let new_note = Object.assign({}, note);
                                  new_note.client_id = -1;
                                  return [new_note, note];
                                } else {
                                  return [note];
                                }
                              });

      let merged = [].concat.apply([], new_holder_array);
      return merged
    case 'KILL_HOLDER_NOTE':
      let removed = state.filter(function(note) { return note.client_id != -1});
      return removed
    case 'FINAL_POSITION_NOTE':
      let final_removed = state.filter(function(note) { return note.client_id != -1});
      return final_removed  
    case 'LOG_PAGE_POS':
      return state.map(function(x) {
        if(x.client_id == action.client_id) {
          let new_pos_note = Object.assign({}, x);
          new_pos_note.page_pos = {pageX: action.pageX, pageY: action.pageY};
          return new_pos_note;
        } else {
          return x;
        }
      })
    default:
      return state
  }
}

const users = (state = [], action) => {
  switch (action.type) {
    case 'INIT_USERS':
      return action.user_list
    default:
      return state
  }
}

const channel = (state = [], action) => {
  return state
}

const chatText = (state = [], action) => {
  switch (action.type) {
    case 'UPDATE_CHAT_TEXT':
      return action.text
    default:
      return state
  }
}



const sections = (state = [], action) => {
  switch (action.type) {
    case 'INIT_SECTIONS':
      return action.section_list
    case 'HOLDER_NOTE':
      //adding the -1 id temp note into sentry list
      let selected_section = state.filter((x)=>{return x.id == action.section_id})[0]
      let holder_sentry_list = selected_section.sentry_list;
      let affected_sentry = holder_sentry_list.filter((x) => {return x.note_client_id == action.client_id})[0]
      let magic_unreachable_sentry_num = -10;

      //putting into an empty list
      if (holder_sentry_list.length == 0) {

        let new_holder_sentry = {id: magic_unreachable_sentry_num, section_id: selected_section.id, note_client_id: -1, next_sentry: -1};
      
        return state.map((x) => {
          if (x.id == selected_section.id) {
            let new_section = Object.assign({},x);
            new_section.sentry_list = [new_holder_sentry];
            new_section.head_sentry_id = magic_unreachable_sentry_num;
            return new_section;
          } else {
            return x;
          }
        })

        //selected note, not in the target section
      } else if (affected_sentry == undefined) {
        let new_holder_sentry = {id: magic_unreachable_sentry_num, section_id: selected_section.id, note_client_id: -1, next_sentry: -1};
        let new_holder_array = []
        //adding a holder note to end as soon  as section enter
        for (var i=0; i < holder_sentry_list.length; i++) {
          if (i < holder_sentry_list.length - 1) {
            new_holder_array.push(holder_sentry_list[i]);
          } else {
            let new_end_sentry = holder_sentry_list[i]
            new_end_sentry.next_sentry = new_holder_sentry.id
            new_holder_array.push(new_end_sentry);
          }
        }
        new_holder_array.push(new_holder_sentry);
        return state.map((x) => {
          if (x.id == selected_section.id) {
            let new_section = Object.assign({},x);
            new_section.sentry_list = new_holder_array;
            return new_section;
          } else {
            return x;
          }
        })


        //moving note within same section
      } else {
        let new_holder_sentry = {id: magic_unreachable_sentry_num, section_id: selected_section.id, note_client_id: -1, next_sentry: affected_sentry.next_sentry};
      
        affected_sentry.next_sentry = new_holder_sentry.id
        
        let new_holder_array = holder_sentry_list.map(function(sentry) {
                                  if (sentry.id == affected_sentry.id) {
                                    return [affected_sentry, new_holder_sentry];
                                  } else {
                                    return [sentry];
                                  }
                                });

        let merged = [].concat.apply([], new_holder_array);
        return state.map((x) => {
          if (x.id == selected_section.id) {
            let new_section = Object.assign({},x);
            new_section.sentry_list = merged;
            return new_section;
          } else {
            return x;
          }
        })
      }
    case 'KILL_HOLDER_NOTE':
      let kill_selected_section = state.filter((x)=>{return x.id == action.section_id})[0]
      let kill_sentry_list = kill_selected_section.sentry_list;
      let unreachable_sentry_num = -10;
      let holder_sentry = kill_sentry_list.filter((x) => {return x.id == unreachable_sentry_num})[0]
      if (holder_sentry == undefined) {
        return state
      } else {
        let pointer_holder = kill_sentry_list.filter((x) => {return x.next_sentry == unreachable_sentry_num})[0]

        let kill_holder_array = kill_sentry_list.map(function(sentry) {
                                  if (sentry.id == holder_sentry.id) {
                                    return [];
                                  } else if ((pointer_holder != undefined) && (sentry.id == pointer_holder.id)) {
                                    let new_pointer = Object.assign({}, pointer_holder);
                                    new_pointer.next_sentry = holder_sentry.next_sentry;
                                    return new_pointer;
                                  } else {
                                    return [sentry];
                                  }
                                });

        let kill_merged = [].concat.apply([], kill_holder_array);
        return state.map((x) => {
          if (x.id == kill_selected_section.id) {
            let new_section = Object.assign({},x);
            new_section.sentry_list = kill_merged;
            if (kill_merged.length == 0) {
              new_section.head_sentry_id = null;
            } else {
              new_section.head_sentry_id = kill_merged[0].id;
            }
            
            return new_section;
          } else {
            return x;
          }
        })
      }
      
    case 'CREATE_NOTE':
      let last_sent_sentry = action.new_section_list.filter((x) =>{return x.note_client_id == action.note.client_id})[0]
      let new_sentry = {id: last_sent_sentry.id, section_id: 1, note_client_id: action.note.client_id, next_sentry: -1};
      let sentry_list = state[0].sentry_list;
      let new_state_zero = sentry_list.map(function(x) {
        if (x.next_sentry == -1) {
          let amended_sentry = Object.assign({}, x);
          amended_sentry.next_sentry = new_sentry.id
          return amended_sentry;
        } else {
          return x;
        }
      });
      new_state_zero.push(new_sentry);

      state[0].head_sentry_id = new_state_zero[0].id
      state[0].sentry_list = new_state_zero;
      return state;

      //now this only moves the holder, movement of the actual note is done in final_position_note
    case 'SWITCH_NOTE_POS':
      let magic_unreachable_sentry_num_2 = -10;
      let holder = {id: magic_unreachable_sentry_num_2, section_id: action.section_id, note_client_id: -1, next_sentry: -1};
      let selfer = null;

      let switch_selected_section = state.filter((x)=>{return x.id == action.section_id})[0]
      let removed_selection = switch_selected_section.sentry_list.map(function(x) {
        if (x.note_client_id == -1) {
          holder = x;
          return [];
        } else if (x.note_client_id == action.selected_id) {
          selfer = x;
          return [x];
        } else {
          return [x];
        }
      });
      let flat_removed = [].concat.apply([], removed_selection);

      let removed_added_back = flat_removed.map(function(x) {
        if (x.note_client_id == action.targeted_id) {
          if (action.pos_string == "bottom") {
            return [x, holder]
          } else {
            return [holder, x]
          }
          
        } else {
          return [x]
        }
      });

      let final_removed = [].concat.apply([], removed_added_back);

      for (var i=0; i < final_removed.length; i++) {
        if (i == (final_removed.length - 1)) {
          final_removed[i].next_sentry = -1;
        } else {
          final_removed[i].next_sentry = final_removed[i + 1].id;
        }
      }

      return state.map((x) => {
        if (x.id == switch_selected_section.id) {
          let new_section = Object.assign({},x);
          new_section.sentry_list = final_removed;
          new_section.head_sentry_id = final_removed[0].id;
          return new_section;
        } else {
          return x;
        }
      });

    case 'FINAL_POSITION_NOTE':
      let magic_unreachable_sentry_num_3 = -10
      let final_holder_sentry = null;
      let sentry_pointer = null;
      let moving_note = null;
      //finding sentry and one before
      state.map(function (section) {
        section.sentry_list.map(function(sentry) {
          if (sentry.id == magic_unreachable_sentry_num_3) {
            final_holder_sentry = sentry;
          }
          if (sentry.next_sentry == magic_unreachable_sentry_num_3) {
            sentry_pointer = sentry;
          }
          if (sentry.note_client_id == action.client_id) {
            moving_note = sentry;
          }
        })
      });

      //leaving note in original pos if no new holder sentry to replace.
      if (final_holder_sentry == null) {
        return state;
      }


      //removing movable note
      state = state.map(function (section) {
        section.sentry_list = section.sentry_list.map(function(sentry) {
                                if (sentry.note_client_id == action.client_id) {
                                  return null;
                                } else {
                                  return sentry;
                                }
                              }).filter(Boolean);
        return section;
      });

      //adding note back in
      state = state.map(function (section) {
        section.sentry_list = section.sentry_list.map(function(sentry) {
                                if (sentry.id == magic_unreachable_sentry_num_3) {
                                  return moving_note;
                                } else {
                                  return sentry;
                                }
                              }).filter(Boolean);
        return section;
      });

      //rewriting the sentry nexts
      state = state.map(function (section) {
        for (var i=0; i < section.sentry_list.length; i++) {
          if (i == (section.sentry_list.length - 1)) {
            section.sentry_list[i].next_sentry = -1;
          } else {
            section.sentry_list[i].next_sentry = section.sentry_list[i + 1].id;
          }
        }

        if (section.sentry_list.length == 0) {  
          section.head_sentry_id = null;
        } else {
          section.head_sentry_id = section.sentry_list[0].id  
        }
        
        return section;
      });

      return state


    case 'CREATE_SECTION':
      return action.section_list
    default:
      return state
  }
}

const messageApp = combineReducers({
  // messagers
  messages,
  notes,
  channel,
  users,
  chatText,
  sections,
  selected_note,
  board_id
})

export default messageApp