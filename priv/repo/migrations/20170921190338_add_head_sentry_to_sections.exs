defmodule Sloth.Repo.Migrations.AddHeadSentryToSections do
  use Ecto.Migration

  def change do
    alter table(:sections) do
      add :head_sentry_id, :integer
    end
  end
end
