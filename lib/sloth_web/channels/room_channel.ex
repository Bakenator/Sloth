defmodule SlothWeb.RoomChannel do
use Phoenix.Channel
alias Sloth.Boards.Message
alias Sloth.Boards.User
alias Sloth.Boards.Note
alias Sloth.Repo
alias Sloth.Presence
require IEx
alias Sloth.Boards.Section
alias Sloth.Accounts.Sentry
alias Sloth.Accounts.Board
import Ecto.Query

  intercept(["new_msg"])

  def join("room:lobby", _message, socket) do

  end

  def join("room:" <> private_room_id, _message, socket) do
    {room_id, _x} = Integer.parse(private_room_id)

    #getting all messages with usernames
    messages = from(p in Message, where: p.board_id == ^room_id)
      |> Sloth.Repo.all
      |> Enum.map(fn(d) -> Sloth.Repo.preload(d,:user) end)
      |> Enum.map(fn(x) -> %{id: x.id, username: x.user.email, content: x.content} end)

    #getting the most recent of all note movements
    notes = from(p in Note, where: p.board_id == ^room_id)
      |> Sloth.Repo.all
      |> Enum.map(fn(x) -> x.client_id end)
      |> Enum.uniq
      |> Enum.map(fn(x) -> Note.most_recent(x) end)
      |> Enum.map(fn(x) -> Note.map_with_assignee(x) end)

    #could move these to page load
    username_list = Repo.all(User) |> Enum.map(fn(x) -> %{id: x.id, email: x.email} end)

    section_list = from(p in Section, where: p.board_id == ^room_id)
                |>  Repo.all
                |>  Enum.map(fn(x) -> %{id: x.id, head_sentry_id: x.head_sentry_id, title: x.title, sentry_list: Sloth.Boards.Section.sentry_list(x, x.head_sentry_id)} end)

    #this is here for the handle_info about precense state
    send(self(), :after_join)

    {:ok, %{room: messages, notes: notes, users: username_list, sections: section_list, board_id: room_id}, socket}
  end


  def join("room:" <> _private_room_id, _params, _socket) do
    {:error, %{reason: "unauthorized"}}
  end



  def handle_info(:after_join, socket) do
    push socket, "presence_state", Presence.list(socket)
    {:ok, _} = Presence.track(socket, socket.assigns.user_id, %{
      online_at: inspect(System.system_time(:seconds))
    })
    {:noreply, socket}
  end



  def handle_in("board_close", %{"board_id" => board_id}, socket) do
    user = Sloth.Repo.get(User, socket.assigns.user_id)

    board = Sloth.Repo.get!(Board, board_id)
    changeset = Board.changeset(board, %{closed: true})
    {:ok, new_board} = Sloth.Repo.update(changeset)
      
    Board.notify_parent_board("close", board.parent_board_id, board.id, user)

    broadcast! socket, "board_close", %{}
    {:noreply, socket}
  end



  def handle_in("new_msg", %{"body" => body, "board_id" => board_id}, socket) do
    user = Sloth.Repo.get(User, socket.assigns.user_id)

    changeset = Message.changeset(%Message{}, %{content: body, user_id: user.id, board_id: board_id })
    {:ok, message} = Sloth.Repo.insert(changeset)

    user_list = User |> Repo.all
    new_mentions = Message.add_user_mention(message, user_list)
    
    broadcast! socket, "new_msg", %{id: message.id, content: body, username: user.email}

    SlothWeb.Endpoint.broadcast("home:lobby", "new_mentions", %{new_mentions: new_mentions})
    #should be able to use handle out here to limit the passing of notifications to other users.

    {:noreply, socket}
  end

  def handle_out("new_msg", payload, socket) do
    push socket, "new_msg", payload
    {:noreply, socket}
  end

  def handle_in("note_move", %{"note_event" => [note_id, newX, newY, text, assignee_id]}, socket) do
    new_note_id = Note.process_new_id(note_id)

    changeset = Note.changeset(%Note{}, %{client_id: new_note_id, posX: newX, posY: newY, text: text, assignee_id: assignee_id})
    {:ok, new_note } = Sloth.Repo.insert(changeset)

    broadcast! socket, "note_move", %{note_event: Note.map_with_assignee(new_note)}
    
    {:noreply, socket}
  end

  def handle_in("note_create", %{"note_event" => %{"note_id" => note_id, "newX" => newX, "newY" => newY, "text" => text, "assignee_id" => assignee_id, "board_id" => board_id}}, socket) do
    new_note_id = Note.process_new_id(note_id)

    changeset = Note.changeset(%Note{}, %{client_id: new_note_id, posX: 10, posY: 10, text: text, assignee_id: assignee_id, board_id: board_id})
    {:ok, new_note} = Sloth.Repo.insert(changeset)

    first_section = Sloth.Repo.one(from x in Section, order_by: [asc: x.id], limit: 1)
    curr_section_list = Sloth.Boards.Section.raw_sentry_list(first_section, first_section.head_sentry_id)
    new_sentry_list = Sloth.Boards.Section.add_note(first_section, curr_section_list, new_note_id)



    broadcast! socket, "note_create", %{new_sentry_list: new_sentry_list, note_event: Note.map_with_assignee(new_note)}

    {:noreply, socket}
  end

  def handle_in("note_assign", %{"note_event" => [note_id, assignee_id]}, socket) do
    new_note_id = Note.process_new_id(note_id)

    old_note = Note.most_recent(note_id)
    changeset = Note.changeset(%Note{}, %{client_id: old_note.client_id, posX: old_note.posX, posY: old_note.posY, text: old_note.text, assignee_id: assignee_id})

    {:ok, new_note} = Sloth.Repo.insert(changeset)

    broadcast! socket, "note_assign", %{note_event: Note.map_with_assignee(new_note)}

    {:noreply, socket}
  end

  def handle_in("sentry_update", %{"section_list" => section_list}, socket) do

    original_section_list = Enum.map(section_list, fn(section) ->
      db_section = Sloth.Repo.get!(Section, section["id"])
      original_note_list = Enum.map(Sloth.Boards.Section.sentry_list(db_section, db_section.head_sentry_id), fn (x) -> 
        x.note_client_id
      end)

      %{board_id: db_section.board_id, note_list: original_note_list, id: db_section.id}
    end)
    original_section_map = Map.new(original_section_list, fn map -> {map.id, map} end)

    note_changes = Enum.map(section_list, fn (section) -> 
      db_section = Sloth.Repo.get!(Section, section["id"])
      sentry_list = section["sentry_list"]
      update_section_sentry(db_section, sentry_list)

      Enum.map(sentry_list, fn (x) -> 
        sentry = Sloth.Repo.get!(Sentry, x["id"])
        sentry_change = Sentry.changeset(sentry, %{next_sentry: x["next_sentry"]})
        Sloth.Repo.update(sentry_change)
      end)

      modified_note_list = Enum.map(sentry_list, fn (x) -> 
        x["note_client_id"]
      end)

      %{board_id: db_section.board_id, title: section["title"], changes: (modified_note_list -- original_section_map[section["id"]].note_list)}
    end)

    changed_note = Enum.filter(note_changes, fn(x) -> length(x.changes) != 0 end)
    Enum.map(changed_note, fn(y) -> 
      [note_id|rest] = y.changes
      user = Sloth.Repo.get(User, socket.assigns.user_id)
      new_mess = Message.changeset(%Message{}, %{user_id: socket.assigns.user_id, board_id: y.board_id, content: "#{user.email} moved note ##{note_id} to #{y.title}"})
      {:ok, out_mess} = Sloth.Repo.insert(new_mess);
      broadcast! socket, "new_msg", %{id: out_mess.id, content: out_mess.content, username: user.email}
    end)

    section_list = 
                    Repo.all(Sloth.Boards.Section)
                |>  Enum.map(fn(x) -> %{id: x.id, board_id: x.board_id, head_sentry_id: x.head_sentry_id, title: x.title, sentry_list: Sloth.Boards.Section.sentry_list(x, x.head_sentry_id)} end)
    

    broadcast! socket, "sentry_update", %{new_sections: section_list}

    {:noreply, socket}
  end

  def update_section_sentry(section, sentry_list) when sentry_list == [] do
    change = Section.changeset(section, %{head_sentry_id: nil})
    Sloth.Repo.update(change)
  end

  def update_section_sentry(section, sentry_list) do
    [first_sentry] = Enum.take(sentry_list, 1)
    change = Section.changeset(section, %{head_sentry_id: first_sentry["id"]})
    Sloth.Repo.update(change)
  end

  def handle_in("section_create", %{"section" => section_map, "board_id" => board_id}, socket) do
    changeset = Section.changeset(%Section{}, %{title: section_map["title"], head_sentry_id: nil, board_id: board_id})
    {:ok, new_section} = Sloth.Repo.insert(changeset)
    section_list = from(p in Section, where: p.board_id == ^board_id)
            |>  Repo.all
            |>  Enum.map(fn(x) -> %{id: x.id, head_sentry_id: x.head_sentry_id, title: x.title, sentry_list: Sloth.Boards.Section.sentry_list(x, x.head_sentry_id)} end)

    broadcast! socket, "section_create", %{section_list: section_list}

    {:noreply, socket}
  end

end