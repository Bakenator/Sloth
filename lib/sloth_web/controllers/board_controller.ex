defmodule SlothWeb.BoardController do
  use SlothWeb, :controller

  alias Sloth.Accounts
  alias Sloth.Accounts.Board

  import Ecto.Query
  import IEx

  def index(conn, _params) do
    boards = Accounts.list_boards()
    render(conn, "index.html", boards: boards)
  end

  def new(conn, _params) do
    changeset = Accounts.change_board(%Board{})
    render(conn, "new.html", changeset: changeset)
  end

  def create(conn, %{"board" => board_params}) do
    case Accounts.create_board(board_params) do
      {:ok, board} ->
        conn
        |> put_flash(:info, "Board created successfully.")
        |> redirect(to: board_path(conn, :show, board))
      {:error, %Ecto.Changeset{} = changeset} ->
        render(conn, "new.html", changeset: changeset)
    end
  end

  def show(conn, %{"id" => name}) do
    [board|rest] = from(p in Sloth.Accounts.Board, where: p.name == ^name) |> Sloth.Repo.all
    render(conn, "show.html", board: board)
  end

  def edit(conn, %{"id" => id}) do
    board = Accounts.get_board!(id)
    changeset = Accounts.change_board(board)
    render(conn, "edit.html", board: board, changeset: changeset)
  end

  def update(conn, %{"id" => id, "board" => board_params}) do
    board = Accounts.get_board!(id)

    case Accounts.update_board(board, board_params) do
      {:ok, board} ->
        conn
        |> put_flash(:info, "Board updated successfully.")
        |> redirect(to: board_path(conn, :show, board))
      {:error, %Ecto.Changeset{} = changeset} ->
        render(conn, "edit.html", board: board, changeset: changeset)
    end
  end

  def delete(conn, %{"id" => id}) do
    board = Accounts.get_board!(id)
    {:ok, _board} = Accounts.delete_board(board)

    conn
    |> put_flash(:info, "Board deleted successfully.")
    |> redirect(to: board_path(conn, :index))
  end
end
