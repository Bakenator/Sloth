// NOTE: The contents of this file will only be executed if
// you uncomment its entry in "assets/js/app.js".

// To use Phoenix channels, the first step is to import Socket
// and connect at the socket path in "lib/web/endpoint.ex":
import {Socket} from "phoenix"
let postSocket = new Socket("/socket", {params: {token: window.userToken}})


postSocket.connect()

// Now that you are connected, you can join channels with a topic:


var channel = postSocket.channel("post:lobby", {})
global_channel = channel;

var blah = 12;

channel.on("post_click", payload => {
  console.log(payload);
})


channel.join()
  .receive("ok", resp => { 
    console.log("Joined successfully", resp) 
  })
  .receive("error", resp => { console.log("Unable to join", resp) })

export default postSocket
