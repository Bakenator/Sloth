defmodule Sloth.Boards.Message do
  use Ecto.Schema
  import Ecto.Changeset
  alias Sloth.Boards.Message
  alias Sloth.Boards.User
  alias Sloth.Boards.UserMention
  require IEx


  schema "messages" do
    field :content, :string
    belongs_to :user, Sloth.Boards.User
    field :board_id, :integer

    timestamps()
  end

  @doc false
  def changeset(%Message{} = message, attrs) do
    message
    |> cast(attrs, [:content, :user_id, :board_id])
    |> validate_required([:content, :user_id, :board_id])
  end

  def add_user_mention(message, user_list) do
    content = message.content
    contained_users = Enum.filter(user_list, fn(x) -> content =~ "@" <> x.email end)
    Enum.map(contained_users, fn(y) -> 
      new_mention = UserMention.changeset(%UserMention{}, %{user_id: y.id, message_id: message.id, board_id: message.board_id})
      {:ok, inserted_mention} = Sloth.Repo.insert(new_mention)
      inserted_mention
    end)
  end

end
