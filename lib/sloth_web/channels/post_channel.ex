defmodule SlothWeb.PostChannel do
use Phoenix.Channel
alias Sloth.Booms.Post
alias Sloth.Booms.Click
alias Sloth.Booms
alias Sloth.Repo
alias Sloth.Presence
require IEx

import Ecto.Query

  def join("post:lobby", _message, socket) do
    user_id = socket.assigns.user_id
    {:ok, %{}, socket}
  end

  def join("home:" <> _private_room_id, _params, _socket) do
    {:error, %{reason: "unauthorized"}}
  end

  def handle_in("post_click", attrs, socket) do

    Booms.create_click(attrs)

    broadcast! socket, "post_click", %{}
    {:noreply, socket}
  end

end