defmodule Sloth.Repo.Migrations.AddBoardIdToMentions do
  use Ecto.Migration

  def change do
    alter table(:usermentions) do
      add :board_id, :integer
    end
  end
end
