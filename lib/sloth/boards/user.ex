defmodule Sloth.Boards.User do
  use Ecto.Schema
  import Ecto.Changeset
  import Ecto.Query
  alias Sloth.Boards.User

  import Doorman.Auth.Bcrypt, only: [hash_password: 1]


  schema "users" do
    field :email, :string
    field :hashed_password, :string
    field :password, :string, virtual: true
    has_many :message, Sloth.Boards.Message

    timestamps()
  end

  @doc false
  def changeset(%User{} = user, attrs) do
    user
    |> cast(attrs, [:email, :hashed_password])
    |> validate_required([:email, :hashed_password])
  end

  def create_changeset(%User{} = user, attrs) do
    user
    |> cast(attrs, ~w(email password))
    |> hash_password
  end
end
