import React from 'react';
import { connect } from 'react-redux'
import Note from './Note';
import HolderNote from './HolderNote';
import BoardNavBar from './BoardNavBar';
import ReactDOM from "react-dom"
import Section from './Section'
import { toggleUserAssign, changeAssignee, holderNote, killHolderNote, logPagePos, switchNotePos, endHolderNote, finalPositionNote } from './actions'

class SectionList extends React.Component {

  sendNewSentryPositions() {
    let section_list = this.props.sections
    this.props.channel.push("sentry_update", {"section_list": section_list})
  }

  render() {
    var that = this;
    const sorted_sections = this.props.sections.sort(function(x,y) {return x.id - y.id})
    const sectionList = sorted_sections.map(function(aSection) {
          return (<Section key={aSection.id} {...aSection}
                    notes={that.props.notes} 
                    addHolderNote={that.props.addHolderNote}
                    finalPositionNote={that.props.finalPositionNote}
                    killHolderNote={that.props.killHolderNote}
                    endHolderNote={that.props.endHolderNote}
                    sendNewSentryPositions={that.sendNewSentryPositions.bind(that)}
                    onUserAssignClick={that.props.onUserAssignClick}
                    switchNotePos={that.props.switchNotePos}
                    selected_note={that.props.selected_note}
                    users={that.props.users}/>)
    });

    return (
      <div className="board_and_nav_holder">
        <BoardNavBar channel={this.props.channel} board_id={this.props.board_id}/>
        <div className="wholeNoteArea">
          {sectionList}
        </div>
      </div>
    );
  }
}

const mapDispatchToProps = dispatch => {
  return {
    onUserToggleClick: id => {
      dispatch(toggleUserAssign(id))
    },
    onUserAssignClick: (client_id, assignee) => {
      dispatch(changeAssignee(client_id, assignee))
    },
    addHolderNote: (section_id, client_id) => {
      dispatch(holderNote(section_id, client_id))
    },
    finalPositionNote: (client_id) => {
      dispatch(finalPositionNote(client_id))
    },
    killHolderNote: (section_id) => {
      dispatch(killHolderNote(section_id))
    },
    logPagePos: (pageX, pageY, client_id) => {
      dispatch(logPagePos(pageX, pageY, client_id))
    },
    switchNotePos: (section_id, selected_id, targeted_id, pos_string) => {
      dispatch(switchNotePos(section_id, selected_id, targeted_id, pos_string))
    },
    endHolderNote: () => {
      dispatch(endHolderNote())
    } 
  }
}

const mapStateToProps = (state) => (state)

export default connect(mapStateToProps, mapDispatchToProps)(SectionList)