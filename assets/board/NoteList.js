import React from 'react';
import { connect } from 'react-redux'
import Note from './Note';
import { toggleUserAssign, changeAssignee } from './actions'

class NoteList extends React.Component {
  render() {
    const noteList = this.props.notes.map((note) => {
                            return (<Note key={note.client_id} {...note} onUserAssignClick={this.props.onUserAssignClick} toggleUserAssign={this.props.onUserToggleClick} users={this.props.users} channel={this.props.channel}/>)
                          })
    return (
      <div>
        {noteList}
      </div>
    );
  }
}

const mapDispatchToProps = dispatch => {
  return {
    onUserToggleClick: id => {
      dispatch(toggleUserAssign(id))
    },
    onUserAssignClick: (client_id, assignee) => {
      
      dispatch(changeAssignee(client_id, assignee))
    }
  }
}

const mapStateToProps = (state) => (state)

export default connect(mapStateToProps, mapDispatchToProps)(NoteList)