defmodule Sloth.Repo.Migrations.CreateSections do
  use Ecto.Migration

  def change do
    create table(:sections) do
      add :title, :string
      add :position, :integer

      timestamps()
    end

  end
end
