defmodule Sloth.Repo.Migrations.CreateSentries do
  use Ecto.Migration

  def change do
    create table(:sentries) do
      add :section_id, :integer
      add :head, :boolean, default: false, null: false
      add :note_client_id, :integer
      add :next_sentry, :integer

      timestamps()
    end

  end
end
