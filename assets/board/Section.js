import React from 'react';
import { connect } from 'react-redux'
import Note from './Note';
import HolderNote from './HolderNote';
import ReactDOM from "react-dom"
import { toggleUserAssign, changeAssignee, holderNote, killHolderNote, logPagePos, switchNotePos } from './actions'

class Section extends React.Component {
  constructor() {
    super();
    this.noteList = [];
  }

  getElement(element) {
    return ReactDOM.findDOMNode(element)
  }

  overSectionArea(e) {
    if (this.props.selected_note == null) {
      return;
    } else {
      console.log('section');
      let selected_id = this.props.selected_note;
      this.props.addHolderNote(this.props.id, selected_id)
      // console.log('checked');
      // let onScreenNotes = this.props.notes.filter((x) => {return x.page_pos != undefined});
      // var that = this;
      // this.props.switchNotePos(this.props.id, selected_id, note.props.client_id, pos_string);
    }
  }

  leaveSectionArea(e) {
    this.props.killHolderNote(this.props.id);
  }

  build_list(curr_list, sentry_list) {
    if (curr_list[curr_list.length - 1] == undefined) {
      var gh = 3;
    }

    if (curr_list[curr_list.length - 1].next_sentry == -1) {
      return curr_list;
    } else {
      let next_id = curr_list[curr_list.length - 1].next_sentry;
      let next_sentry = (sentry_list.filter((x) => {return x.id == next_id}))[0];
      // let next_sentry = (sentry_list.filter((x) => {return x.next_sentry == next_id}))[0];
      return (curr_list.push(next_sentry), sentry_list);
    }
  }

  build_note_list(curr_list, ordered_sentry, notes) {
    if (ordered_sentry.length == 0) {
      return curr_list;
    } else {
      //adding next ordered note to list
      curr_list.push((notes.filter((x) => {return x.client_id == ordered_sentry[0].note_client_id}))[0]);
      return this.build_note_list(curr_list, ordered_sentry.slice(1, ordered_sentry.length), notes);
    }
  }

  build_section_list() {
      let sentry_list = this.props.sentry_list;
      if ((sentry_list.length == 0) || (this.props.head_sentry_id == null)) {
        return [];
      }

      let section_head = this.props.head_sentry_id;
      let list_start = (sentry_list.filter((x) => {return x.id == section_head}))[0]
      //section_id: 1, note_client_id: 1, next_sentry: -1,
      if (list_start == undefined) {
        var tr = 3;
        // list_start = sentry_list[0];
      }
      let ordered_sentry_list = this.build_list([list_start], sentry_list)
      let note_list = this.build_note_list([], ordered_sentry_list, this.props.notes)
      return note_list;
  }

  checkSwitchNote(note, pos_string) {
    //need to alter to grab the moving note
    if (this.props.selected_note == null) {
      return;
    } else {
      let selected_id = this.props.selected_note;
      console.log('checked');
      let onScreenNotes = this.props.notes.filter((x) => {return x.page_pos != undefined});
      var that = this;
      this.props.switchNotePos(this.props.id, selected_id, note.props.client_id, pos_string);
    }
  }

  render() {
          const note_list = this.build_section_list();
          const note_order = note_list.map((x) => { return x.client_id });
          var that = this;
          const noteList = note_list.map((note) => {
                            if (note.client_id == -1) {
                              return (<HolderNote key={-1}/>)
                            } else {
                              return (<Note key={note.client_id} {...note} 
                                        noteOrder={note_order}
                                        sendNewSentryPositions={that.props.sendNewSentryPositions}
                                        addHolderNote={that.props.addHolderNote} 
                                        sectionId={this.props.id}
                                        finalPositionNote={that.props.finalPositionNote}
                                        killHolderNote={that.props.killHolderNote}
                                        endHolderNote={that.props.endHolderNote}
                                        checkSwitchNote={that.checkSwitchNote.bind(that)}
                                        // logPagePos={that.props.logPagePos}
                                        onUserAssignClick={that.props.onUserAssignClick} 
                                        // toggleUserAssign={that.props.onUserToggleClick} 
                                        users={that.props.users} 
                                        channel={that.props.channel}/>)
                            }
                          });

        return (
          <div>
            <div className="section_title">
              <h3>{this.props.title}</h3>
            </div>
            <div className="section_holder" onMouseLeave={this.leaveSectionArea.bind(this)} onMouseEnter={this.overSectionArea.bind(this)}>
              {noteList}
            </div>
          </div>  
        )
  }
}

export default Section