defmodule Sloth.Accounts.Sentry do
  use Ecto.Schema
  import Ecto.Changeset
  import Ecto.Query
  alias Sloth.Accounts.Sentry
  require IEx


  schema "sentries" do
    field :head, :boolean, default: false
    field :next_sentry, :integer, default: -1
    field :note_client_id, :integer
    field :section_id, :integer
    field :board_id, :integer

    timestamps()
  end

  @doc false
  def changeset(%Sentry{} = sentry, attrs) do
    sentry
    |> cast(attrs, [:board_id, :section_id, :head, :note_client_id, :next_sentry])
    |> validate_required([:board_id, :section_id, :head, :note_client_id])
  end

  def get_sentry_list(next_sentry_id, curr_list) when next_sentry_id == -1 do
    curr_list
  end

  def get_sentry_list(next_sentry_id, curr_list \\ []) do
    next_sentry = Sloth.Repo.one(from x in Sentry, where: x.id == ^next_sentry_id, order_by: [desc: x.id], limit: 1)
    stripped_sentry = Map.delete(Map.from_struct(next_sentry), :__meta__)  
    Sentry.get_sentry_list(next_sentry.next_sentry, curr_list ++ [stripped_sentry])
  end


  def get_raw_sentry_list(next_sentry_id, curr_list) when next_sentry_id == -1 do
    curr_list
  end

  def get_raw_sentry_list(next_sentry_id, curr_list \\ []) do
    next_sentry = Sloth.Repo.one(from x in Sentry, where: x.id == ^next_sentry_id, order_by: [desc: x.id], limit: 1)
    Sentry.get_sentry_list(next_sentry.next_sentry, curr_list ++ [next_sentry])
  end

end
