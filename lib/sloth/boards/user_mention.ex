defmodule Sloth.Boards.UserMention do
  use Ecto.Schema
  import Ecto.Changeset
  alias Sloth.Boards.UserMention


  schema "usermentions" do
    field :message_id, :integer
    field :user_id, :integer
    field :board_id, :integer

    timestamps()
  end

  @doc false
  def changeset(%UserMention{} = user_mention, attrs) do
    user_mention
    |> cast(attrs, [:user_id, :message_id, :board_id])
    |> validate_required([:user_id, :message_id, :board_id])
  end
end
