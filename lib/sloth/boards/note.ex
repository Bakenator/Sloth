defmodule Sloth.Boards.Note do
  use Ecto.Schema
  import Ecto.Changeset
  import Ecto.Query

  alias Sloth.Boards.Note
  alias Sloth.Boards.User
  require IEx

  schema "notes" do
    field :client_id, :integer
    field :posX, :integer
    field :posY, :integer
    field :text, :string, default: ""
    field :assignee_id, :integer, default: -1
    field :board_id, :integer

    timestamps()
  end

  @doc false
  def changeset(%Note{} = note, attrs) do
    note
    |> cast(attrs, [:board_id, :posX, :posY, :text, :client_id, :assignee_id])
    |> validate_required([:board_id, :posX, :posY, :client_id, :assignee_id])
  end

  def most_recent(client_id) do
    query = from q in Note, where: q.client_id == ^client_id, order_by: q.inserted_at
    Sloth.Repo.all(query)
    |> Enum.reverse
    |> List.first
    # |> (&(%{client_id: &1.client_id, posX: &1.posX, posY: &1.posY, text: &1.text})).()
  end

  def next_client_id() do
    Sloth.Repo.all(Note)
    |> Enum.map(fn(x) -> x.client_id end)
    |> (&(&1 ++ [0])).()
    |> Enum.max
    |> (&(&1 + 1)).()
  end

  def process_new_id(note_id) when note_id < 0 do
    next_client_id
  end

  def process_new_id(note_id) do
    note_id
  end

  def map_with_assignee(%Note{} = note) do
    user = Note.assignee(note.assignee_id)
    note_map = Map.merge((Map.from_struct note), %{assignee: %{id: user.id, email: user.email}})
    Map.delete(note_map, :__meta__)  
  end

  def assignee(assignee_id) when assignee_id == -1 do
    %{id: -1, email: "None"}
  end

  def assignee(assignee_id) do
    Map.from_struct(Sloth.Repo.get(User, assignee_id))
  end

end
