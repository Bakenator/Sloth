import React from 'react';
import ReactDOM from "react-dom"
import ContentEditable from "react-contenteditable";
import UserDropDownList from "./UserDropDownList"

// https://stackoverflow.com/questions/20926551/recommended-way-of-making-react-component-div-draggable

class BoardNavBar extends React.Component {

  homeClick() {
    window.location = '/home';
  }

  closeBoard() {
    this.props.channel.push("board_close", {board_id: this.props.board_id});
  }

  render() {
    return (
              <div className="board_nav_bar">
                <p className="board_nav_home_button" onClick={this.homeClick}>Home</p>
                <p className="board_nav_home_button" onClick={this.closeBoard.bind(this)}>Close Board</p>
              </div>
            ) 
  }
}

export default BoardNavBar