export function process_tags(text, user_list) {
    let text_pieces = text.split(' ');
    let new_text_arr = text_pieces.map(color_word);
    new_text_arr = new_text_arr.map((x) => { return color_username(x, user_list) }) 

    return new_text_arr.join(' ');
  }

export function  color_word(word) {
    if (!(word.includes('@addnote'))) {
      return word.split(/>|</)[2] || word;
    } else {
      return '<span>' + (word.split(/>|</)[2] || word) + '</span>';
    }
  }

export function color_username(word, user_list) {
    var glob_name = user_list.map((user) => {return user.email});
    let new_word = word;
    for (name of glob_name) {
      if (!(word.includes('@' + name))) {
        
      } else {
        new_word = '<span>' + (word.split(/>|</)[2] || word) + '</span>';
      }
    }
    return new_word;
  }