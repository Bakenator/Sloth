import React from 'react';
import Draggable from 'react-draggable';
import ReactDOM from "react-dom"
import ContentEditable from "react-contenteditable";
import UserDropDownList from "./UserDropDownList"

// https://stackoverflow.com/questions/20926551/recommended-way-of-making-react-component-div-draggable

class Note extends React.Component {
  constructor(props) {
    super(props);
    this.divStyle = {
      // position: "absolute",
      left: props.posX + "px",
      top: props.posY + "px",
      margin: 5 + "px"
    };
    this.state = {
      deltaPosition: {
        x: 0, y: 0
      },
      dragging: false
    };

    this.docMove = this.onMouseMove.bind(this);
    this.docUp = this.onMouseUp.bind(this);
  }

  getElement(element) {
    return ReactDOM.findDOMNode(this)
  }

  // calculate relative position to the mouse and set dragging=true
  onMouseDown (e) {
    this.setState({
      dragging: true,
      position: {
        x: this.getElement(this).getBoundingClientRect().left,
        y: this.getElement(this).getBoundingClientRect().top
      },
      rel: {
        x: e.pageX,
        y: e.pageY
      }
    })

    this.divStyle = {
        position: "absolute",
        zIndex: "10",
        pointerEvents: "none",
        left: (this.state.position.x) + "px",
        top: (this.state.position.y) + "px"
    };

    this.props.addHolderNote(this.props.sectionId, this.props.client_id)
  }

  onMouseUp (e) {
    this.setState({dragging: false});
    this.handleStop();
    this.divStyle = {
        position: "static",
        margin: 5 + "px"
    };

    
    this.props.finalPositionNote(this.props.client_id)
    // this.props.killHolderNote(this.props.sectionId)
    this.props.endHolderNote();
    this.props.sendNewSentryPositions();
  }

  onMouseMove (e) {
    if (this.state == undefined) {
      x = 4;
    }
    if (this.state.dragging) {
      e.preventDefault();
      e.stopPropagation()
      // console.log('hi');
      const {x, y} = this.state.deltaPosition;
      this.setState({
        deltaPosition: {
          x: (e.pageX - this.state.rel.x),
          y: (e.pageY - this.state.rel.y),
        }
      });

      const pageX = this.state.position.x + this.state.deltaPosition.x;
      const pageY = this.state.position.y + this.state.deltaPosition.y;

      this.divStyle = {
          position: "absolute",
          zIndex: "10",
          pointerEvents: "none",
          left: pageX + "px",
          top: pageY + "px"
      };

      // this.props.checkSwitchNote(pageX, pageY, this);
    }
  }

  runMouseOver(e) {
    
    let distFromTop = e.clientY - e.currentTarget.getBoundingClientRect().top;
    if (distFromTop < 100) {
      // console.log(distFromTop);
      this.props.checkSwitchNote(this, "top");  
    } else {
      this.props.checkSwitchNote(this, "bottom");
    }
    // console.log(this.props.client_id);
  }

  componentDidMount() {
    this.setState({
      rel: {
        x: 0,
        y: 0
      },
      position: {
        x: this.getElement(this).getBoundingClientRect().left,
        y: this.getElement(this).getBoundingClientRect().top
      }
    });

    // this.props.logPagePos(this.getElement(this).getBoundingClientRect().left, this.getElement(this).getBoundingClientRect().top, this.props.client_id)
  }

  //had to add in case mouse exits the draggable area due to speed.
  componentDidUpdate(props, state) {
    if (this.state.dragging && !state.dragging) {
      document.addEventListener('mousemove', this.docMove);
      document.addEventListener('mouseup', this.docUp);
    } else if (!this.state.dragging && state.dragging) {
      document.removeEventListener('mousemove', this.docMove);
      document.removeEventListener('mouseup', this.docUp);
    } 
  }

  handleStop() {
    let newX = this.props.posX + this.state.deltaPosition.x;
    let newY = this.props.posY + this.state.deltaPosition.y;
    // this.props.channel.push("note_move", {note_event: [this.props.client_id, newX, newY, this.props.text, this.props.assignee_id]});

    this.setState({
      deltaPosition: {
        x: 0,
        y: 0
      }
    });
  }

  componentWillReceiveProps(nextProps) {
    // var x = 3;
    // let a1 = nextProps.noteOrder;
    // let a2 = this.props.noteOrder;
    // //comparing scalar arrays
    // if (!(a1.length==a2.length && a1.every(function(v,i) { return v === a2[i]}))) {
    //   console.log('hi');
    //   this.props.logPagePos(this.getElement(this).getBoundingClientRect().left, this.getElement(this).getBoundingClientRect().top, this.props.client_id)
    // }
  }

  //this is for the content editable
  emitChange(e) {
    this.props.channel.push("note_move", {note_event: [this.props.client_id, this.props.posX, this.props.posY, e.target.value, this.props.assignee_id]});
  }

  sendToggle() {
    this.props.toggleUserAssign(this.props.client_id)
  }

  render() {
    // https://github.com/mzabriskie/react-draggable#draggable-api
    // https://github.com/lovasoa/react-contenteditable
    var className = 'dropdown-content' + (this.props.user_toggle ? '' : ' dropdown-content-hidden');

    return (
              <div style={this.divStyle} onMouseMove={this.runMouseOver.bind(this)}>
                <div className="topBar" onMouseDown={this.onMouseDown.bind(this)} onMouseUp={this.onMouseUp.bind(this)} onMouseMove={this.onMouseMove.bind(this)}></div>
                <div className="form">
                  <div className="textAria">
                    <ContentEditable
                      className="inner_note_text"
                      html={this.props.text} // innerHTML of the editable div
                      disabled={false}       // use true to disable edition
                      onChange={this.emitChange.bind(this)} // handle innerHTML change
                    />
                    <div className="note_assigned_user">
                      <div className="spacer"></div>
                      <div className="user_text dropbtn" onClick={this.sendToggle.bind(this)}>{this.props.assignee.email}</div>
                      <div className={className}>
                        <UserDropDownList onUserAssignClick={this.props.onUserAssignClick} channel={this.props.channel} client_id={this.props.client_id} toggle={this.props.user_toggle} users={this.props.users} />
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            ) 
  }
}

export default Note

