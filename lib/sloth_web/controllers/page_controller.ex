defmodule SlothWeb.PageController do
  use SlothWeb, :controller

  def index(conn, _params) do
    user = conn.assigns[:current_user]
    render conn, "index.html"
  end

  def home(conn, _params) do
    user = conn.assigns[:current_user]
    render conn, "home.html"
  end
end
