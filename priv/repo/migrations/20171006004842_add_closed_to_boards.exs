defmodule Sloth.Repo.Migrations.AddClosedToBoards do
  use Ecto.Migration

  def change do
    alter table(:boards) do
      add :closed, :boolean, default: false
    end
  end
end
