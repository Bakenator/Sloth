defmodule SlothWeb.SessionController do
  use SlothWeb, :controller

  alias Sloth.Boards
  alias Sloth.Boards.Session
  alias Sloth.Boards.User

  import Doorman.Login.Session, only: [login: 2]

  def index(conn, _params) do
    sessions = Boards.list_sessions()
    render(conn, "index.html", sessions: sessions)
  end

  def new(conn, _params) do
    changeset = Boards.change_session(%Session{})
    render(conn, "new.html", changeset: changeset)
  end

  # def create(conn, %{"session" => session_params}) do
  #   case Boards.create_session(session_params) do
  #     {:ok, session} ->
  #       conn
  #       |> put_flash(:info, "Session created successfully.")
  #       |> redirect(to: session_path(conn, :show, session))
  #     {:error, %Ecto.Changeset{} = changeset} ->
  #       render(conn, "new.html", changeset: changeset)
  #   end
  # end

  def create(conn, %{"session" => %{"email" => email, "password" => password}}) do
    if user = Doorman.authenticate(email, password) do
      conn
      |> login(user) # Sets :user_id on conn's session
      |> put_flash(:notice, "Successfully logged in")
      |> redirect(to: "/")
    else
      conn
      |> put_flash(:error, "No user found with the provided credentials")
      |> render("new.html")
    end
  end

  def show(conn, %{"id" => id}) do
    session = Boards.get_session!(id)
    render(conn, "show.html", session: session)
  end

  def edit(conn, %{"id" => id}) do
    session = Boards.get_session!(id)
    changeset = Boards.change_session(session)
    render(conn, "edit.html", session: session, changeset: changeset)
  end

  def update(conn, %{"id" => id, "session" => session_params}) do
    session = Boards.get_session!(id)

    case Boards.update_session(session, session_params) do
      {:ok, session} ->
        conn
        |> put_flash(:info, "Session updated successfully.")
        |> redirect(to: session_path(conn, :show, session))
      {:error, %Ecto.Changeset{} = changeset} ->
        render(conn, "edit.html", session: session, changeset: changeset)
    end
  end

  def delete(conn, %{"id" => id}) do
    session = Boards.get_session!(id)
    {:ok, _session} = Boards.delete_session(session)

    conn
    |> put_flash(:info, "Session deleted successfully.")
    |> redirect(to: session_path(conn, :index))
  end
end
