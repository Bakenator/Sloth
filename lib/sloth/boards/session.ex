defmodule Sloth.Boards.Session do
  use Ecto.Schema
  import Ecto.Changeset
  alias Sloth.Boards.Session


  schema "sessions" do

    timestamps()
  end

  @doc false
  def changeset(%Session{} = session, attrs) do
    session
    |> cast(attrs, [])
    |> validate_required([])
  end
end
