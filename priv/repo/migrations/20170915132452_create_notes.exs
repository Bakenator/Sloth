defmodule Sloth.Repo.Migrations.CreateNotes do
  use Ecto.Migration

  def change do
    create table(:notes) do
      add :posX, :integer
      add :posY, :integer
      add :text, :string
      add :client_id, :integer

      timestamps()
    end

  end
end
