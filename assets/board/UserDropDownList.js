import React from 'react';
import UserDropDownRow from './UserDropDownRow'

class UserDropDownList extends React.Component {

  render() {
    var rows = [];
    rows = this.props.users.map((user) => { return <UserDropDownRow key={user.id}  channel={this.props.channel} client_id={this.props.client_id} user={user} onUserAssignClick={this.props.onUserAssignClick}/>})
    return (<ul>{rows}</ul>);
  }
}

export default UserDropDownList

  //     for (name in glob_name) {
  //   let a = document.createElement("a");
  //   a.id = 'new_assignee_' + client_id + '_' + glob_name[parseInt(name)].id;
  //   a.innerHTML = glob_name[parseInt(name)].email; 
  //   document.getElementById('myDropdown' + client_id).appendChild(a);
  // }