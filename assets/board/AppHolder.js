import React from 'react';
import { connect } from 'react-redux'
import MessageList from './MessageList';
import NoteList from './NoteList';
import SectionList from './SectionList';

class AppHolder extends React.Component {
  render() {
    return (
      <div className="reactRoot">
        <SectionList />
        <MessageList />
      </div>
    );
  }
}

export default AppHolder