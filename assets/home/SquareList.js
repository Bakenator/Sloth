import React from 'react';
import { connect } from 'react-redux'
import ReactDOM from "react-dom"
import BoardSquare from "./BoardSquare"
import HomeNavBar from "./HomeNavBar"
import RecurSquare from './RecurSquare'

class SquareList extends React.Component {

  buildBoardList(boards) {
    let top = boards.filter((x)=> {return x.parent_board_id == null});
    return top.map((r)=> {return this.addSubBoards(r, boards)});
  }

  addSubBoards(board, board_list) {
    let children = board_list.filter((x)=> {return x.parent_board_id == board.id});
    if (children.length == 0) {
      board.children = [];
      return board;
    }

    board.children = children.map((y)=> {return this.addSubBoards(y, board_list)})
    return board;  
  }
  
  render() {
    const recur_list = this.buildBoardList(this.props.boards);

    const squaresList = recur_list.map((board) => {
                            return (
                              <div key={board.id} className="recur_list_holder"> 
                                <RecurSquare 
                                  mentions={this.props.mentions} 
                                  childrener={board.children} 
                                  channel={this.props.channel} 
                                  {...board} />
                              </div>
                            )
                          })

    return ( 
      <div>
        <HomeNavBar channel={this.props.channel}/>
        <div className="recur_all_holder">
          {squaresList}
        </div>
      </div>  
    );
  }
}

const mapStateToProps = (state) => (state)

const mapDispatchToProps = dispatch => {
  return {
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(SquareList)