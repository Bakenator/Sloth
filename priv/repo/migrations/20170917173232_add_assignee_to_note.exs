defmodule Sloth.Repo.Migrations.AddAssigneeToNote do
  use Ecto.Migration

  def change do
    alter table(:notes) do
      add :assignee_id, :integer
    end
  end
end
