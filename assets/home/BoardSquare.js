import React from 'react';
import { connect } from 'react-redux'
// import { toggleUserAssign, changeAssignee } from './actions'

class BoardSquare extends React.Component {
  constructor(props) {
    super(props);
    this.getMentionCount = this.getMentionCount.bind(this);
  }

  gotoBoard() {
    window.location = '/boards/'+ this.props.name;
  }

  addSubBoard() {
    this.props.channel.push("new_board", {parent_id: this.props.id});
  }

  getMentionCount(mentions) {
    let board_id = this.props.id;
    if (mentions != undefined) {
      let board_mentions = mentions.filter((x)=> {return (x.board_id == board_id)});
      return (<p>{board_mentions.length}</p>)
    } else {
      return (<p></p>);
    }
  }



  render() {
    const that = this;
    const section_infos = this.props.section_counts.map(function(x) {
      return (<div key={x.title + that.props.name}>{x.title}: {x.length}</div>);
    })

    return (
      <div className="board_square_holder">
        <div className="board_square" onClick={this.gotoBoard.bind(this)}>
          <p>{this.props.name}</p>
          {this.getMentionCount(this.props.mentions)}
        </div>
        <div className="board_info_square">
          {section_infos}
        </div>
        <div className="sub_board_button" onClick={this.addSubBoard.bind(this)}>
          Add A Sub Board
        </div>
      </div>
    );
  }
}

export default BoardSquare