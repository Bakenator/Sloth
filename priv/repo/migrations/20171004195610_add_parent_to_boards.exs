defmodule Sloth.Repo.Migrations.AddParentToBoards do
  use Ecto.Migration

  def change do
    alter table(:boards) do
      add :parent_board_id, :integer, default: nil
    end
  end
end
