defmodule Sloth.Booms.Post do
  use Ecto.Schema
  import Ecto.Changeset
  alias Sloth.Booms.Post


  schema "posts" do
    field :title, :string

    timestamps()
  end

  @doc false
  def changeset(%Post{} = post, attrs) do
    post
    |> cast(attrs, [:title])
    |> validate_required([:title])
  end
end
