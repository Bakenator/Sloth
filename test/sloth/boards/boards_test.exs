defmodule Sloth.BoardsTest do
  use Sloth.DataCase

  alias Sloth.Boards

  describe "notes" do
    alias Sloth.Boards.Note

    @valid_attrs %{client_id: 42, posX: 42, posY: 42, text: "some text"}
    @update_attrs %{client_id: 43, posX: 43, posY: 43, text: "some updated text"}
    @invalid_attrs %{client_id: nil, posX: nil, posY: nil, text: nil}

    def note_fixture(attrs \\ %{}) do
      {:ok, note} =
        attrs
        |> Enum.into(@valid_attrs)
        |> Boards.create_note()

      note
    end

    test "list_notes/0 returns all notes" do
      note = note_fixture()
      assert Boards.list_notes() == [note]
    end

    test "get_note!/1 returns the note with given id" do
      note = note_fixture()
      assert Boards.get_note!(note.id) == note
    end

    test "create_note/1 with valid data creates a note" do
      assert {:ok, %Note{} = note} = Boards.create_note(@valid_attrs)
      assert note.client_id == 42
      assert note.posX == 42
      assert note.posY == 42
      assert note.text == "some text"
    end

    test "create_note/1 with invalid data returns error changeset" do
      assert {:error, %Ecto.Changeset{}} = Boards.create_note(@invalid_attrs)
    end

    test "update_note/2 with valid data updates the note" do
      note = note_fixture()
      assert {:ok, note} = Boards.update_note(note, @update_attrs)
      assert %Note{} = note
      assert note.client_id == 43
      assert note.posX == 43
      assert note.posY == 43
      assert note.text == "some updated text"
    end

    test "update_note/2 with invalid data returns error changeset" do
      note = note_fixture()
      assert {:error, %Ecto.Changeset{}} = Boards.update_note(note, @invalid_attrs)
      assert note == Boards.get_note!(note.id)
    end

    test "delete_note/1 deletes the note" do
      note = note_fixture()
      assert {:ok, %Note{}} = Boards.delete_note(note)
      assert_raise Ecto.NoResultsError, fn -> Boards.get_note!(note.id) end
    end

    test "change_note/1 returns a note changeset" do
      note = note_fixture()
      assert %Ecto.Changeset{} = Boards.change_note(note)
    end
  end

  describe "messages" do
    alias Sloth.Boards.Message

    @valid_attrs %{content: "some content"}
    @update_attrs %{content: "some updated content"}
    @invalid_attrs %{content: nil}

    def message_fixture(attrs \\ %{}) do
      {:ok, message} =
        attrs
        |> Enum.into(@valid_attrs)
        |> Boards.create_message()

      message
    end

    test "list_messages/0 returns all messages" do
      message = message_fixture()
      assert Boards.list_messages() == [message]
    end

    test "get_message!/1 returns the message with given id" do
      message = message_fixture()
      assert Boards.get_message!(message.id) == message
    end

    test "create_message/1 with valid data creates a message" do
      assert {:ok, %Message{} = message} = Boards.create_message(@valid_attrs)
      assert message.content == "some content"
    end

    test "create_message/1 with invalid data returns error changeset" do
      assert {:error, %Ecto.Changeset{}} = Boards.create_message(@invalid_attrs)
    end

    test "update_message/2 with valid data updates the message" do
      message = message_fixture()
      assert {:ok, message} = Boards.update_message(message, @update_attrs)
      assert %Message{} = message
      assert message.content == "some updated content"
    end

    test "update_message/2 with invalid data returns error changeset" do
      message = message_fixture()
      assert {:error, %Ecto.Changeset{}} = Boards.update_message(message, @invalid_attrs)
      assert message == Boards.get_message!(message.id)
    end

    test "delete_message/1 deletes the message" do
      message = message_fixture()
      assert {:ok, %Message{}} = Boards.delete_message(message)
      assert_raise Ecto.NoResultsError, fn -> Boards.get_message!(message.id) end
    end

    test "change_message/1 returns a message changeset" do
      message = message_fixture()
      assert %Ecto.Changeset{} = Boards.change_message(message)
    end
  end

  describe "users" do
    alias Sloth.Boards.User

    @valid_attrs %{email: "some email", hashed_password: "some hashed_password"}
    @update_attrs %{email: "some updated email", hashed_password: "some updated hashed_password"}
    @invalid_attrs %{email: nil, hashed_password: nil}

    def user_fixture(attrs \\ %{}) do
      {:ok, user} =
        attrs
        |> Enum.into(@valid_attrs)
        |> Boards.create_user()

      user
    end

    test "list_users/0 returns all users" do
      user = user_fixture()
      assert Boards.list_users() == [user]
    end

    test "get_user!/1 returns the user with given id" do
      user = user_fixture()
      assert Boards.get_user!(user.id) == user
    end

    test "create_user/1 with valid data creates a user" do
      assert {:ok, %User{} = user} = Boards.create_user(@valid_attrs)
      assert user.email == "some email"
      assert user.hashed_password == "some hashed_password"
    end

    test "create_user/1 with invalid data returns error changeset" do
      assert {:error, %Ecto.Changeset{}} = Boards.create_user(@invalid_attrs)
    end

    test "update_user/2 with valid data updates the user" do
      user = user_fixture()
      assert {:ok, user} = Boards.update_user(user, @update_attrs)
      assert %User{} = user
      assert user.email == "some updated email"
      assert user.hashed_password == "some updated hashed_password"
    end

    test "update_user/2 with invalid data returns error changeset" do
      user = user_fixture()
      assert {:error, %Ecto.Changeset{}} = Boards.update_user(user, @invalid_attrs)
      assert user == Boards.get_user!(user.id)
    end

    test "delete_user/1 deletes the user" do
      user = user_fixture()
      assert {:ok, %User{}} = Boards.delete_user(user)
      assert_raise Ecto.NoResultsError, fn -> Boards.get_user!(user.id) end
    end

    test "change_user/1 returns a user changeset" do
      user = user_fixture()
      assert %Ecto.Changeset{} = Boards.change_user(user)
    end
  end

  describe "sessions" do
    alias Sloth.Boards.Session

    @valid_attrs %{}
    @update_attrs %{}
    @invalid_attrs %{}

    def session_fixture(attrs \\ %{}) do
      {:ok, session} =
        attrs
        |> Enum.into(@valid_attrs)
        |> Boards.create_session()

      session
    end

    test "list_sessions/0 returns all sessions" do
      session = session_fixture()
      assert Boards.list_sessions() == [session]
    end

    test "get_session!/1 returns the session with given id" do
      session = session_fixture()
      assert Boards.get_session!(session.id) == session
    end

    test "create_session/1 with valid data creates a session" do
      assert {:ok, %Session{} = session} = Boards.create_session(@valid_attrs)
    end

    test "create_session/1 with invalid data returns error changeset" do
      assert {:error, %Ecto.Changeset{}} = Boards.create_session(@invalid_attrs)
    end

    test "update_session/2 with valid data updates the session" do
      session = session_fixture()
      assert {:ok, session} = Boards.update_session(session, @update_attrs)
      assert %Session{} = session
    end

    test "update_session/2 with invalid data returns error changeset" do
      session = session_fixture()
      assert {:error, %Ecto.Changeset{}} = Boards.update_session(session, @invalid_attrs)
      assert session == Boards.get_session!(session.id)
    end

    test "delete_session/1 deletes the session" do
      session = session_fixture()
      assert {:ok, %Session{}} = Boards.delete_session(session)
      assert_raise Ecto.NoResultsError, fn -> Boards.get_session!(session.id) end
    end

    test "change_session/1 returns a session changeset" do
      session = session_fixture()
      assert %Ecto.Changeset{} = Boards.change_session(session)
    end
  end
end
