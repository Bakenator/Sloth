import React from 'react';

class ChatMessage extends React.Component {
  createMarkup() {
    return {__html: this.props.username + ': ' + this.props.content};
  }

  render() {
    return <p className="shown_message" dangerouslySetInnerHTML={this.createMarkup()}></p>
  }
}

export default ChatMessage
