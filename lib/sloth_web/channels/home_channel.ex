defmodule SlothWeb.HomeChannel do
use Phoenix.Channel
alias Sloth.Boards.Message
alias Sloth.Boards.User
alias Sloth.Boards.Note
alias Sloth.Boards.UserMention
alias Sloth.Repo
alias Sloth.Presence
require IEx
alias Sloth.Boards.Section
alias Sloth.Accounts.Sentry
alias Sloth.Accounts.Board
import Ecto.Query

  intercept(["new_msg"])
  intercept(["new_mentions"])

  def join("home:lobby", _message, socket) do
    user_id = socket.assigns.user_id

    boards = Board.get_boards_for_home()
    mentions = UserMention 
      |> where([p], p.user_id == ^user_id)
      |> Repo.all
    {:ok, %{boards: boards, mentions: mentions}, socket}
  end


  def join("home:" <> _private_room_id, _params, _socket) do
    {:error, %{reason: "unauthorized"}}
  end

  def handle_out("new_mentions", payload, socket) do
    #filtering to only the current_users events
    new_mentions = %{new_mentions: Enum.filter(payload.new_mentions, &(&1.user_id == socket.assigns.user_id))}
    push socket, "new_mentions", payload
    {:noreply, socket}
  end

  def handle_info(:after_join, socket) do
    push socket, "presence_state", Presence.list(socket)
    {:ok, _} = Presence.track(socket, socket.assigns.user_id, %{
      online_at: inspect(System.system_time(:seconds))
    })
    {:noreply, socket}
  end



  def handle_in("new_board", %{"parent_id" => parent_id}, socket) do
    changeset = Board.changeset(%Board{}, %{parent_board_id: parent_id, name: ("New Board " <> Integer.to_string(Enum.random(0..100)))})
    {:ok, _board} = Sloth.Repo.insert(changeset)

    boards = Board.get_boards_for_home()
    broadcast! socket, "update_boards", %{boards: boards}
    {:noreply, socket}
  end

  def handle_in("new_board", %{}, socket) do
    changeset = Board.changeset(%Board{}, %{name: ("New Board " <> Integer.to_string(Enum.random(0..100)))})
    {:ok, _board} = Sloth.Repo.insert(changeset)

    boards = Board.get_boards_for_home()
    broadcast! socket, "update_boards", %{boards: boards}
    {:noreply, socket}
  end


end