defmodule Sloth.Booms.Click do
  use Ecto.Schema
  import Ecto.Changeset
  alias Sloth.Booms.Click


  schema "clicks" do
    field :post_id, :integer

    timestamps()
  end

  @doc false
  def changeset(%Click{} = click, attrs) do
    click
    |> cast(attrs, [:post_id])
    |> validate_required([:post_id])
  end
end
