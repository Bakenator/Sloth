import React from 'react';

class UserDropDownRow extends React.Component {
  userClick() {
    this.props.channel.push("note_assign", {note_event: [this.props.client_id, this.props.user.id]});
    // this.props.onUserAssignClick(this.props.client_id, this.props.user)
  }

  render() {
    return <a key={this.props.user.id} onClick={this.userClick.bind(this)}>{this.props.user.email}</a>
  }
}

export default UserDropDownRow