import React from 'react';
import { connect } from 'react-redux'
import BoardSquare from './BoardSquare'
// import { toggleUserAssign, changeAssignee } from './actions'

class RecurSquare extends React.Component {

  render() {
    const that = this;
    const children = this.props.childrener;
    const child_square_list = children.map((x)=> {
      return (<RecurSquare 
                key={x.id} 
                childrener={x.children} 
                channel={this.props.channel} 
                mentions={this.props.mentions}
                {...x} />)}
    );

    return (
      <div className="board_square_holder">
        <div className="squares_holder">
          <BoardSquare 
            key={this.props.id} 
            channel={this.props.channel} 
            mentions={this.props.mentions}
            {...this.props} />
        </div>
        <div className="subBoards">
          {child_square_list}
        </div>
      </div>
    );
  }
}

export default RecurSquare