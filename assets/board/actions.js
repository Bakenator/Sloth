let nextTodoId = 0
export const initMessages = message_list => {
  return {
    type: 'INIT_MESSAGES',
    message_list: message_list
  }
}

export const addMessage = message => {
  return {
    type: 'ADD_MESSAGE',
    message
  }
}

export const setVisibilityFilter = filter => {
  return {
    type: 'SET_VISIBILITY_FILTER',
    filter
  }
}

export const toggleTodo = id => {
  return {
    type: 'TOGGLE_TODO',
    id
  }
}



export const initNotes = note_list => {
  return {
    type: 'INIT_NOTES',
    note_list: note_list
  }
}

export const moveNote = note => {
  return {
    type: 'MOVE_NOTE',
    note
  }
}

export const createNote = (new_section_list, note) => {
  return {
    type: 'CREATE_NOTE', 
    note: note,
    new_section_list: new_section_list
  }
}


export const logPagePos = (pageX, pageY, client_id) => {
  return {
    type: 'LOG_PAGE_POS',
    pageX: pageX,
    pageY: pageY,
    client_id: client_id
  }
}


export const holderNote = (section_id, client_id) => {
  return {
    type: 'HOLDER_NOTE',
    client_id: client_id,
    section_id: section_id
  }
}

export const killHolderNote = (section_id) => {
  return {
    type: 'KILL_HOLDER_NOTE',
    section_id
  }
}

export const endHolderNote = () => {
  return {
    type: 'END_HOLDER_NOTE'
  }
}

export const initUsers = user_list => {
  return {
    type: 'INIT_USERS',
    user_list: user_list
  }
}


export const toggleUserAssign = id => {
  return {
    type: 'TOGGLE_USER_ASSIGN',
    note_id: id
  }
}


export const changeAssignee = (client_id, assignee) => {
  return {
    type: 'CHANGE_ASSIGNEE',
    client_id: client_id,
    assignee: assignee
  }
}

export const updateChatText = (text) => {
  return {
    type: 'UPDATE_CHAT_TEXT',
    text: text
  }
}



export const initSections = section_list => {
  return {
    type: 'INIT_SECTIONS',
    section_list: section_list
  }
}

export const createSection = (new_section_list) => {
  return {
    type: 'CREATE_SECTION', 
    section_list: new_section_list
  }
}

export const switchNotePos = (section_id, selected_id, targeted_id, pos_string) => {
  return {
    type: 'SWITCH_NOTE_POS',
    section_id: section_id,
    selected_id: selected_id,
    targeted_id: targeted_id, 
    pos_string: pos_string
  }
}

export const finalPositionNote = (client_id) => {
  return {
    type: 'FINAL_POSITION_NOTE',
    client_id
  }
}




export const initBoardId = board_id => {
  return {
    type: 'INIT_BOARD_ID',
    board_id
  }
}