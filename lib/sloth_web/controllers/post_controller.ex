defmodule SlothWeb.PostController do
  use SlothWeb, :controller
  alias Sloth.Booms.Post
  alias Sloth.Booms.Click
  alias Sloth.Booms

  import IEx

  def index(conn, _params) do
    user = conn.assigns[:current_user]
    changeset = Post.changeset(%Post{}, %{})

    clicks = Sloth.Repo.all(Click)
      |> Enum.map(fn x -> x.post_id end)
      |> Enum.group_by(fn x -> x end)

    posts = Sloth.Repo.all(Post)

    render(conn, "index.html", changeset: changeset, posts: posts, clicks: clicks)
  end

  def new(conn, _params) do
    changeset = Post.changeset(%Post{}, %{})
    render(conn, "new.html", changeset: changeset)
  end

  def create(conn, %{"post" => post_params}) do
    case Booms.create_post(post_params) do
      {:ok, post} ->
        conn
        |> put_flash(:info, "Post created successfully.")
        |> redirect(to: post_path(conn, :index))
      {:error, %Ecto.Changeset{} = changeset} ->
        render(conn, "new.html", changeset: changeset)
    end
  end

  def show(conn, %{"id" => id}) do
  end

  def edit(conn, %{"id" => id}) do
  end

  def update(conn, %{"id" => id, "post" => post_params}) do
  end

  def delete(conn, %{"id" => id}) do
  end

end
